from django.shortcuts import get_object_or_404, render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodolistForm, TodoitemForm
from django.views.generic.detail import DetailView

# Create your views here.
def todolists(request):
    lists = TodoList.objects.all()
    context = {
        "lists": lists
    }
    return render(request, 'todos/todos_list.html', context)

def todolist(request, pk):
    context = {
        "list": TodoList.objects.get(pk=pk) if TodoList else None
    }

    return render(request, 'todos/todo_list.html', context)

def createtodolist(request):
    if request.method == "POST" and TodoList:
        form = TodolistForm(request.POST)

        if form.is_valid():
            todolist = form.save()

            return redirect("todo_list_detail", pk=todolist.pk)

    elif TodolistForm:# If the form is not valid, keep the form 
        form = TodolistForm()

    else:# If there is no todolist form then it's set to none
        form = None

    context = {
        "form": form,
    }        
    # remain on the create page
    return render(request, "todos/create_todo_list.html", context)


def updatetodolist(request, pk):
    context = {}
    todolist = get_object_or_404(TodoList, pk=pk)
    form = TodolistForm(request.POST or None, instance=todolist)

    if form.is_valid():
        form.save()
        return redirect("todo_list_detail", pk=todolist.pk)

    context["form"] = form
    return render(request, "todos/update_todo_list.html", context)

def deletetodolist(request, pk):
    context = {}
    todolist = get_object_or_404(TodoList, pk=pk)
    if request.method == "POST" and TodoList:
        todolist.delete()
        return redirect("todo_list_list")
    
    return render(request, 'todos/delete_todo_list.html', context)

def createtodoitem(request):
    context = {}
    if request.method == 'POST' and TodoItem:
        form = TodoitemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", pk=item.list.pk)
    elif TodoItem:
        form = TodoitemForm()
    else:
        form: None

    context = {
        "form": form
    }

    return render(request, "todos/create_todo_item.html", context)

def updatetodoitem(request, pk):
    context = {}
    todoitem = get_object_or_404(TodoItem, pk=pk)
    form = TodoitemForm(request.POST or None, instance=todoitem)

    if form.is_valid():
        form.save()
        return redirect("todo_list_detail", pk=todoitem.list.pk)

    context["form"] = form
    return render(request, "todos/update_todo_item.html", context)