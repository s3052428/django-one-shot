from django import forms
from todos.models import TodoItem, TodoList

class TodolistForm(forms.ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class TodoitemForm(forms.ModelForm):
    class Meta:
        model = TodoItem
        fields = ["task", "due_date", "is_completed", "list"]
