from django.urls import path
from todos.views import (
    todolists,
    todolist,
    createtodolist,
    updatetodolist,
    deletetodolist,
    createtodoitem,
    updatetodoitem,
)


urlpatterns = [
    path('', todolists, name="todo_list_list"),
    path('<int:pk>/', todolist, name="todo_list_detail"),
    path('create/', createtodolist, name="todo_list_create"),
    path('<int:pk>/edit/', updatetodolist, name="todo_list_update"),
    path('<int:pk>/delete/', deletetodolist, name="todo_list_delete"),
    path('items/create/', createtodoitem, name="todo_item_create"),
    path('items/<int:pk>/edit/', updatetodoitem, name="todo_item_update"),
]
